'use strict'

const express = require('express');
const pgp = require('pg-promise');
const app = express();
const { port } = require('./config');
const db = require('./db');
app.use(express.json());

const api = require('./routes/api');

app.listen(port, () => {
    console.log(`App listen on port: ${port}`);
});

app.post('/createUser', async(req, res) => {
    try {
        
        const { username, password, name, email, address } = req.body;

        let query = pgp.as.format(
            'SELECT MAX("id") FROM "User"'
        );

        const max_id = await db.oneOrNone(query);

        query = pgp.as.format(
            'INSERT INTO "User" ("id", "username", "pass", "name", "email", "address") ' +
            ' VALUES ($1, $2, $3, $4, $5, $6) RETURNING *',
            [max_id.max + 1, username, password, name, email, address]
        );

        const user = await db.oneOrNone(query);

        if(!user){
            return res.sendStatus(501);
        }

        return res.sendStatus(200);

    } catch (error) {
        console.log(error.stack);
        return res.send(501)
    }
});

app.use(async(req, res, next) => {
    try {

        const { username, password } = req.query;

        if(!username || !password){
            return res.status(403).json({ msg: 'No username or password' });
        }

        let query = pgp.as.format(
            'SELECT * FROM "User" WHERE "username" = $1 AND "pass" = $2',
            [username, password]
        );

        const result = await db.oneOrNone(query);

        if(!result){
            return res.sendStatus(403).json({ msg: 'Sorry, you are not allowed' });
        }

        next();

    } catch (error) {
        console.log(error.stack);
        return res.sendStatus(501);
    }
});

app.use('/api', api);