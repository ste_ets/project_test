const config = require('./config');
const initOption = {
    error: (error, e) => {
        console.log(error);
    }
}

const pgp = require('pg-promise')(initOption);
const dbconnString = config.db_url;
const db = pgp({connectionString: dbconnString, max: 2, ssl: {rejectUnauthorized: false}});

module.exports = db;