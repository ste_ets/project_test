'use strict'

const express = require('express');
const router = express.Router();
const db = require('../db');
const pgp = require('pg-promise');

router.get('/getAllUsers', async(req, res) => {
    try {
        
        let query = pgp.as.format(
            'SELECT * FROM "User"'
        );

        const result = await db.any(query);

        return res.json({ data: result });

    } catch (error) {
        console.log(error.stack);
        return res.send(501).json({ msg: 'Something went wrong' });
    }
});

router.delete('/deleteUser', async (req, res) => {
    try {
        const { id } = req.body;
        if (!id) {
            return res.sendStatus(403);
        }

        let query = pgp.as.format(
            'DELETE FROM "User" WHERE "id" = $1 RETURNING *',
            id
        );

        const result = await db.oneOrNone(query);

        if (!result) {
            return res.send(501).json({ msg: 'No user delete' });
        }

        return res.sendStatus(200);


    } catch (error) {
        console.log(error.stack);
        return res.sendStatus(501);
    }
});

router.put('/updateUser', async (req, res) => {
    try {
        const { id, name, email, address } = req.body;

        if (!id || !name || !email || !address) {
            return res.send(403).json({ msg: 'You are not enabled' });
        }

        let query = pgp.as.format(
            'UPDATE "User" SET "name" = $1, "email" = $2, "address" = $3 WHERE "id" = $4 RETURNING *',
            [name, email, address, id]
        );

        const update_user = await db.oneOrNone(query);

        if (!update_user) {
            return res.send(501).json({ msg: 'No user update' });
        }

        return res.sendStatus(200);

    } catch (error) {
        console.log(error.stack);
        return res.sendStatus(501);
    }
});

module.exports = router;