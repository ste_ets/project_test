CREATE TABLE "User" (
    id INTEGER PRIMARY KEY,
    username VARCHAR(20),
    pass VARCHAR(20),
    name VARCHAR(20),
    email VARCHAR(20),
    address VARCHAR(20)
)